﻿using Dalamud.Configuration;
using Dalamud.Plugin;
using SpecialSnowflakes.Chara;
using System;
using System.Collections.Generic;

namespace SpecialSnowflakes.Utils
{
    [Serializable]
    public class Configuration : IPluginConfiguration
    {
        public int Version { get; set; } = 1;

        public bool SomePropertyToBeSavedAndWithADefault { get; set; } = true;
        public bool zoomScaling { get; set; } = false;
        public bool allowDutyDynamicZoom { get; set; } = false;
        public bool readOnlyHeights { get; set; } = false;
        public RefreshMode refreshBoolOption { get; set; } = RefreshMode.DISABLED;
        public int refreshIntervallUpdate { get; set; } = 5;
        public List<Character> characterList { get; set; } = new List<Character>();

        // the below exist just to make saving less cumbersome

        [NonSerialized]
        private DalamudPluginInterface pluginInterface;

        public void Initialize(DalamudPluginInterface pluginInterface)
        {
            this.pluginInterface = pluginInterface;
        }

        public void Save()
        {
            pluginInterface.SavePluginConfig(this);
        }
    }
}
