﻿using SpecialSnowflakes.Utils;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Threading;

public unsafe class Character : IEquatable<Character>
{
    public string Name { get; set; }
    public float Height { get; set; }
    public bool IsSitting { get; set; }
    public bool ReadOnlyHeight { get; set; }
    public bool DisableCustomScale { get; set; }
    public Vector3 LastKnownPos { get; set; }
    private byte* LastKnownCharacterBase { get; set; }
    private byte* LastKnownModelView { get; set; }


    public Character(string charName, float height)
    {
        Name = charName;
        Height = height;
        IsSitting = false;
        ReadOnlyHeight = false;
        DisableCustomScale = false;
        LastKnownPos = new Vector3(0, 0, 0);
    }

    public bool Equals([AllowNull] Character other)
    {
        if (other == null) return false;
        return Name == other.Name;
    }

    public byte* UpdateLastKnownAddresses(byte* characterBase)
    {
        LastKnownCharacterBase = characterBase;
        LastKnownModelView = MemoryManager.GetModelView(characterBase);
        return LastKnownCharacterBase;
    }
    public byte* GetLastKnownCharacterBase()
    {
        return LastKnownCharacterBase;
    }
    public byte* GetLastKnownModelView()
    {
        return LastKnownModelView;
    }
    public void ClearCharacterAddresses()
    {
        LastKnownCharacterBase = null;
        LastKnownModelView = null;
    }
    public void ResetLastKnownPos()
    {
        LastKnownPos = new Vector3(0, 0, 0);
    }
}