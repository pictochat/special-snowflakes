﻿using Dalamud.Game.ClientState.Objects.Enums;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Logging;
using SpecialSnowflakes.States;
using SpecialSnowflakes.Utils;
using System;
using System.Numerics;
using System.Threading;
using System.Timers;

namespace SpecialSnowflakes.Chara
{
    public enum RefreshMode : int
    {
        DISABLED = 0,
        INTERVAL = 1,
        ALWAYS = 2,
    };
    public class RefreshState
    {
        public RefreshMode CurrentMode { get; set; }
        public int IntervalRate = 5;
        public RefreshState(Configuration config)
        {
            CurrentMode = config.refreshBoolOption;
            IntervalRate = config.refreshIntervallUpdate;
        }
    }
    internal class CharacterRefreshManager : IDisposable
    {
        private const float CAM_Y_SITTING_POS = 1.46733f;
        private const float CAM_Z_SITTING_POS = 0.17f;

        private const int BASE_SCALE_ADDR_OFFSET = 0x274;
        private const int BASE_CUSTOM_SCALE_ADDR_OFFSET = 0x074;
        private const int CAM_ZOOM_MIN_ADDR_OFFSET = 0x118;
        private const int CAM_ZOOM_MAX_ADDR_OFFSET = 0x11C;
        private const int CAM_Y_ANGLE_ADDR_OFFSET = 0x224;
        private const int CAM_Y_SITTING_POS_ADDR_OFFSET = 0x184;
        private const int CAM_Z_SITTING_POS_ADDR_OFFSET = 0x188;

        public RefreshState CurrentRefreshState { get; init; }

        private SSFCharacterManager characterManager { get; init; }
        private PluginPreferences pluginPreferences { get; init; }
        private PluginState pluginState { get; init; }

        private System.Timers.Timer refreshTimer { get; set; }

        public CharacterRefreshManager(Configuration config, SSFCharacterManager characterManager, PluginState pluginState, PluginPreferences preferences)
        {
            this.pluginState = pluginState;
            this.characterManager = characterManager;

            pluginPreferences = preferences;
            CurrentRefreshState = new RefreshState(config);
            refreshTimer = new System.Timers.Timer();

            RefreshAllHeights();
            RestartRefresh();
        }

        // Logic for refreshing heights.
        public void RestartRefresh()
        {
            refreshTimer.Stop();
            if (CurrentRefreshState.CurrentMode != RefreshMode.INTERVAL || CurrentRefreshState.IntervalRate < 1) return;
            PluginLog.Debug($"Restarting auto-refresh with {CurrentRefreshState.IntervalRate} seconds.");
            refreshTimer = new System.Timers.Timer();
            refreshTimer.Elapsed += new ElapsedEventHandler(RefreshTrigger);
            refreshTimer.Interval = 1000 * CurrentRefreshState.IntervalRate;
            refreshTimer.Start();
        }
        public void RefreshTrigger(object source, ElapsedEventArgs e)
        {
            RefreshAllHeights();
        }
        public unsafe void RefreshNextHeight()
        {
            if (!pluginState.IsLoggedIn()) return;
            if (characterManager.IsCharacterQueueEmpty()) return;

            Character character = characterManager.CharacterQueue.Dequeue();
            UpdateCharacterBase(character);
            if (!character.ReadOnlyHeight) UpdateHeight(character);
            UpdateCameraHeight(character);
            UpdateSittingHeight(character);
            characterManager.CharacterQueue.Enqueue(character);
        }
        public unsafe void RefreshAllHeights(bool readOnly = false)
        {
            if (!pluginState.IsLoggedIn()) return;
            foreach (Character character in characterManager.Characters)
            {
                UpdateCharacterBase(character);
                if (!readOnly) UpdateHeight(character);
                UpdateCameraHeight(character);
                UpdateSittingHeight(character);
            }
        }
        public void StopRefresh()
        {
            if (CurrentRefreshState.CurrentMode == RefreshMode.INTERVAL)
            {
                RestartRefresh();
            }
            else
            {
                refreshTimer.Stop();
            }
        }

        // Logic for character updating, might break out.
        public unsafe byte* UpdateCharacterBase(Character character)
        {
            byte* characterBase = null;
            bool inGpose = characterManager.ObjectTable.GetObjectAddress(201) != IntPtr.Zero;

            foreach (GameObject obj in characterManager.ObjectTable)
            {
                if (inGpose && obj.ObjectIndex < 201 || !inGpose && obj.ObjectIndex > 200)
                {
                    continue;
                }
                if (obj.ObjectKind == ObjectKind.Player && obj.Name.ToString().Equals(character.Name))
                {
                    characterBase = (byte*)obj.Address.ToPointer();
                }
            }
            return characterBase !=  null ? character.UpdateLastKnownAddresses(characterBase) : null;
        }
        public unsafe float GetCurrentHeight(byte* modelViewBaseAddr)
        {
            float currentHeight = 1.0f;
            MemoryManager.ReadFloatValue(modelViewBaseAddr, BASE_SCALE_ADDR_OFFSET, &currentHeight);
            return currentHeight;
        }
        public unsafe float GetCustomScale(byte* modelViewBaseAddr)
        {
            float currentScale = 1.0f;
            MemoryManager.ReadFloatValue(modelViewBaseAddr, BASE_CUSTOM_SCALE_ADDR_OFFSET, &currentScale);
            return currentScale;
        }

        public unsafe void UpdateHeight(Character character)
        {
            if (character.Height <= 0) return;

            float storedHeight = character.Height;

            byte* characterBaseAddr = character.GetLastKnownCharacterBase();
            byte* modelViewAddr = character.GetLastKnownModelView();
            if (characterBaseAddr == null || modelViewAddr == null)
            {
                UpdateCharacterBase(character);
                return;
            }

            if (storedHeight == GetCurrentHeight(modelViewAddr)) return;

            MemoryManager.WriteFloatValue(&storedHeight, modelViewAddr, BASE_SCALE_ADDR_OFFSET);
        }
        private unsafe bool isSitting(byte* characterBaseAddr)
        {
            // Sitting poses for reference: 0x32, 0x5F, 0x60, 0xFE, 0xFF
            int[] sittingPoses = { 0x32, 0x5F, 0x60, 0xFE, 0xFF };
            byte gameEmote = 0x00;
            Buffer.MemoryCopy(characterBaseAddr + 0x634, &gameEmote, sizeof(byte), sizeof(byte));
            foreach (byte pose in sittingPoses)
            {
                if (pluginState.GetLocalPlayer() != null && pluginState.GetLocalPlayer().Address.ToPointer() == characterBaseAddr
                    && pluginState.GetLocalPlayer().StatusFlags.HasFlag(StatusFlags.WeaponOut)) return false;
                if (pose == gameEmote) return true;
            }
            return false;
        }
        public unsafe void UpdateSittingHeight(Character character, bool sittingTriggered = false)
        {
            byte* characterBaseAddr = character.GetLastKnownCharacterBase();
            byte* modelViewAddr = character.GetLastKnownModelView();
            if (characterBaseAddr == null || modelViewAddr == null)
            {
                return;
            }

            float currentHeight = character.ReadOnlyHeight ? GetCurrentHeight(modelViewAddr) : character.Height;
            if (!character.DisableCustomScale) currentHeight *= GetCustomScale(modelViewAddr);

            character.IsSitting = sittingTriggered || isSitting(characterBaseAddr);
            if (character.IsSitting)
            {
                float xAdjust = 0, yAdjust = 0, zAdjust = 0;
                float q1 = 0, q2 = 0, q3 = 0, q4 = 0;

                // 0x50, 0x54, 0x58 Are address offsets for character position.
                // 0x60, 0x64, 0x68, 0x6c Are address offsets for character rotation.
                MemoryManager.ReadFloatValue(modelViewAddr, 0x50, &xAdjust);
                MemoryManager.ReadFloatValue(modelViewAddr, 0x54, &yAdjust);
                MemoryManager.ReadFloatValue(modelViewAddr, 0x58, &zAdjust);
                MemoryManager.ReadFloatValue(modelViewAddr, 0x60, &q1);
                MemoryManager.ReadFloatValue(modelViewAddr, 0x64, &q2);
                MemoryManager.ReadFloatValue(modelViewAddr, 0x68, &q3);
                MemoryManager.ReadFloatValue(modelViewAddr, 0x6C, &q4);

                // Math for moving character to the chair
                float mag = (float)Math.Sqrt(q2 * q2 + q4 * q4);
                q2 /= mag;
                float finalAngle = 2f * (float)Math.Acos(q2) + (float)Math.PI / 4f;

                if (character.LastKnownPos != new Vector3(xAdjust, yAdjust, zAdjust))
                {
                    float xAbsolute = 0.15f + currentHeight * -0.15f;
                    float yAbsolute = 0.15f + currentHeight * -0.15f;
                    xAdjust += (float)(xAbsolute * Math.Cos(finalAngle) - yAbsolute * Math.Sin(finalAngle));
                    zAdjust += (float)(xAbsolute * Math.Sin(finalAngle) + yAbsolute * Math.Cos(finalAngle));
                    yAdjust += characterManager.GetRacialModifier(character) + currentHeight * -0.5f;
                    MemoryManager.WriteFloatValue(&xAdjust, modelViewAddr, 0x50);
                    MemoryManager.WriteFloatValue(&yAdjust, modelViewAddr, 0x54);
                    MemoryManager.WriteFloatValue(&zAdjust, modelViewAddr, 0x58);
                }
                character.LastKnownPos = new Vector3(xAdjust, yAdjust, zAdjust);
            }
        }

        public unsafe void UpdateCameraHeight(Character character)
        {
            if (pluginState.GetLocalPlayer() == null || !pluginState.GetLocalPlayer().Name.ToString().Equals(character.Name))
            {
                return;
            }
            if (pluginPreferences.AllowZoomAdjustments && !(pluginPreferences.DisllowDutyZoomAdjustments && pluginState.InDutyOrCombat))
            {
                byte* characterBase = character.GetLastKnownCharacterBase();
                byte* modelView = character.GetLastKnownModelView();
                if (characterBase == null || modelView == null)
                {
                    UpdateCharacterBase(character);
                    return;
                }
                float currentHeight = character.ReadOnlyHeight ? GetCurrentHeight(modelView) : character.Height;
                float currentScale = character.DisableCustomScale ? 1.0f : GetCustomScale(modelView);

                if (currentHeight == 0) return;
                if (CameraSettings.CameraZoomDelta == 0.75f * currentHeight * currentScale) return; //easy way to tell if height changed;
                PluginLog.Debug("Updating camera");

                CameraSettings.CameraZoomDelta = 0.75f * currentHeight * currentScale;
                float cameraZoomMin = 1.5f * currentHeight * currentScale;
                float cameraZoomMax = 20f * currentHeight * currentScale;
                float cameraUpDown = -0.22867f * currentHeight;


                if (pluginState.CameraSettings.CameraManagerAddress != IntPtr.Zero)
                {

                    MemoryManager.WriteFloatValue(&cameraZoomMin, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_ZOOM_MIN_ADDR_OFFSET);
                    MemoryManager.WriteFloatValue(&cameraZoomMax, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_ZOOM_MAX_ADDR_OFFSET);
                    if (!isSitting(characterBase))
                    {
                        /*
                        float yCamAdjust = 0.0f;
                        float* yCamAdjustAddr = &yCamAdjust;
                        Buffer.MemoryCopy(characterBase + 0x184, yCamAdjustAddr, sizeof(float), sizeof(float));
                        PluginLog.Debug($"Y value: {yCamAdjust}");

                        yCamAdjust *= currentScale;
                        Buffer.MemoryCopy(&yCamAdjust, characterBase + 0x184, sizeof(float), sizeof(float));
                        */

                        MemoryManager.WriteFloatValue(&cameraUpDown, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_Y_ANGLE_ADDR_OFFSET);
                    }
                    else
                    {
                        float yCamAdjust = CAM_Y_SITTING_POS * 0.5f * currentHeight;
                        MemoryManager.WriteFloatValue(&yCamAdjust, characterBase, CAM_Y_SITTING_POS_ADDR_OFFSET);
                        float zCamAdjust = Math.Max(CAM_Z_SITTING_POS, CAM_Z_SITTING_POS * currentHeight);
                        MemoryManager.WriteFloatValue(&zCamAdjust, characterBase, CAM_Z_SITTING_POS_ADDR_OFFSET);
                    }
                }
            }
        }
        public unsafe void ResetCameraDefaults()
        {
            // CAMERA DEFAULTS
            float cameraZoomMin = 1.5f;
            float cameraZoomMax = 20f;
            float cameraUpDown = -0.21952191f;
            CameraSettings.CameraZoomDelta = 0.75f;

            SpecialSnowflakes.SetCameraHookState(pluginPreferences.AllowZoomAdjustments);

            if (pluginState.CameraSettings.CameraManagerAddress != IntPtr.Zero)
            {
                MemoryManager.WriteFloatValue(&cameraZoomMin, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_ZOOM_MIN_ADDR_OFFSET);
                MemoryManager.WriteFloatValue(&cameraZoomMax, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_ZOOM_MAX_ADDR_OFFSET);
                MemoryManager.WriteFloatValue(&cameraUpDown, (byte*)pluginState.CameraSettings.CameraManagerAddress.ToPointer(), CAM_Y_ANGLE_ADDR_OFFSET);
            }
        }

        public void Dispose()
        {
            refreshTimer.Stop();
            refreshTimer.Dispose();
        }
    }
}
