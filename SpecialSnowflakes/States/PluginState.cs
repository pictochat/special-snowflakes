﻿using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Objects.SubKinds;
using System;

namespace SpecialSnowflakes.States
{
    public class CameraSettings
    {
        public static float CameraZoomDelta = 0.75f;
        public IntPtr CameraManagerAddress { get; set; }
        public CameraSettings()
        {
        }
    }
    internal class PluginState
    {
        private ClientState ClientState { get; init; }

        public bool InDutyOrCombat { get; set; }
        public CameraSettings CameraSettings { get; set; }
        public PluginState(ClientState clientState)
        {
            ClientState = clientState;
            CameraSettings = new CameraSettings();
        }
        public PlayerCharacter GetLocalPlayer()
        {
            return ClientState.LocalPlayer;
        }

        public bool IsLoggedIn()
        {
            return ClientState.IsLoggedIn;
        }

    }
}
