﻿using Dalamud.Game.Command;
using Dalamud.IoC;
using Dalamud.Plugin;
using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Objects;
using Dalamud.Game.ClientState.Conditions;
using System.Reflection;
using SpecialSnowflakes.Utils;
using System.Collections.Generic;

namespace SpecialSnowflakes
{
    public sealed class Plugin : IDalamudPlugin
    {
        public string Name => "Special Snowflakes";

        private const string commandName = "/ssf";

        private DalamudPluginInterface PluginInterface { get; init; }
        private CommandManager CommandManager { get; init; }
        private SpecialSnowflakes ssf { get; init; }

        public Plugin(
            [RequiredVersion("1.0")] DalamudPluginInterface pluginInterface,
            [RequiredVersion("1.0")] CommandManager commandManager,
            [RequiredVersion("1.0")] ClientState clientState,
            [RequiredVersion("1.0")] Framework framework,
            [RequiredVersion("1.0")] ObjectTable objects,
            [RequiredVersion("1.0")] Condition condition,
            [RequiredVersion("1.0")] SigScanner sigscanner)
        {
            PluginInterface = pluginInterface;
            CommandManager = commandManager;

            Configuration config = PluginInterface.GetPluginConfig() as Configuration ?? new Configuration();
            config.Initialize(pluginInterface);

            ssf = new SpecialSnowflakes(
                clientState,
                condition,
                framework,
                objects,
                sigscanner,
                config
            );

            var assemblyLocation = Assembly.GetExecutingAssembly().Location;

            CommandManager.AddHandler(commandName, new CommandInfo(ssf.OnCommand)
            {
                HelpMessage = "Open the configuraiton for Special Snowflakes\n/ssf off → Disable height refreshing\n/ssf always, /ssf on → Always enable height refreshing\n/ssf interval → Enable interval height refreshing\n/ssf refresh → Refresh all heights now."
            });

            PluginInterface.UiBuilder.Draw += ssf.DrawUI;
            PluginInterface.UiBuilder.OpenConfigUi += ssf.DrawConfigUI;
        }

        public void Dispose()
        {
            ssf.Dispose();
            CommandManager.RemoveHandler(commandName);
            PluginInterface.UiBuilder.Draw -= ssf.DrawUI;
            PluginInterface.UiBuilder.OpenConfigUi -= ssf.DrawConfigUI;
        }

    }
}
