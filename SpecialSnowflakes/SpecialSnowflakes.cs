﻿using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Conditions;
using Dalamud.Game.ClientState.Objects;
using Dalamud.Hooking;
using Dalamud.Logging;
using FFXIVClientStructs.FFXIV.Client.Game.Character;
using System;

using SpecialSnowflakes.Chara;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpecialSnowflakes.UI;
using SpecialSnowflakes.Utils;
using SpecialSnowflakes.States;

namespace SpecialSnowflakes
{
    public class PluginPreferences
    {
        public bool AllowZoomAdjustments = false;
        public bool DisllowDutyZoomAdjustments = false;
        public PluginPreferences(Configuration config)
        {
            AllowZoomAdjustments = config.zoomScaling;
            DisllowDutyZoomAdjustments = config.allowDutyDynamicZoom;
        }
    }
    internal class SpecialSnowflakes
    {
        // Constants
        private const int CHARACTER_SEARCH_TIMEOUT = 5000;

        // Dalamud
        private ClientState clientState { get; init; }
        private Condition condition { get; init; }
        private Configuration configuration { get; init; }
        private Framework framework { get; init; }
        private SigScanner sigScanner { get; init; }

        private PluginState PluginState { get; init; }
        private PluginUI PluginUi { get; init; }

        // Main Controller
        private PluginPreferences pluginPreferences { get; init; }
        private SSFCharacterManager characterManager { get; init; }
        private CharacterRefreshManager characterRefreshManager { get; init; }
        private delegate float getCameraZoomDeltaDelegate();
        private static Hook<getCameraZoomDeltaDelegate> getCameraZoomDeltaHook;
        private static float getCameraZoomDeltaDetour() => CameraSettings.CameraZoomDelta;

        public SpecialSnowflakes(
            ClientState clientState,
            Condition condition,
            Framework framework,
            ObjectTable objectTable,
            SigScanner sigScanner,
            Configuration config)
        {
            this.clientState = clientState;
            this.condition = condition;
            this.framework = framework;
            this.sigScanner = sigScanner;
            configuration = config;

            PluginState = new PluginState(clientState);
            pluginPreferences = new PluginPreferences(configuration);
            initCameraSettings();
            characterManager = new SSFCharacterManager(configuration, objectTable);
            characterRefreshManager = new CharacterRefreshManager(configuration, characterManager, PluginState, pluginPreferences);

            PluginUi = new PluginUI(configuration, characterManager, characterRefreshManager, pluginPreferences);

            framework.Update += OnUpdateEvent;
            this.clientState.Logout += onLogoutEvent;
        }

        private void onLogoutEvent(object? sender, EventArgs? args)
        {
            PluginLog.Log("Logout detected, clearing cached addresses.");
            characterManager.ReinitCharacterAddresses();
        }

        public void OnUpdateEvent(object framework)
        {
            if (condition[ConditionFlag.BetweenAreas51]
             || condition[ConditionFlag.BetweenAreas]
             || condition[ConditionFlag.OccupiedInCutSceneEvent])
            {
                characterRefreshManager.ResetCameraDefaults();
                return;
            }
            if (pluginPreferences.AllowZoomAdjustments && pluginPreferences.DisllowDutyZoomAdjustments)
            {
                if (condition[ConditionFlag.InCombat] ||
                    condition[ConditionFlag.BoundByDuty])
                {
                    PluginState.InDutyOrCombat = true;
                    characterRefreshManager.ResetCameraDefaults();
                }
                else
                {
                    PluginState.InDutyOrCombat = false;
                }
            }
            if (characterRefreshManager.CurrentRefreshState.CurrentMode != RefreshMode.ALWAYS) return;
            if (characterManager.IsCharacterQueueEmpty()) characterManager.InitQueue();
            characterRefreshManager.RefreshNextHeight();
        }
        private unsafe void initCameraSettings()
        {
            byte* tempAddr;
            IntPtr cameraManagerSignatureAddr;
            if (sigScanner.TryGetStaticAddressFromSig("4C 8D 35 ?? ?? ?? ?? 85 D2", out cameraManagerSignatureAddr, 0) == false)
            {
                PluginLog.Debug($"Camera manager could not be found. Disabling hook.");
                return;
            }
            Buffer.MemoryCopy(cameraManagerSignatureAddr.ToPointer(), &tempAddr, sizeof(byte*), sizeof(byte*));
            PluginState.CameraSettings.CameraManagerAddress = new IntPtr(tempAddr);
            IntPtr* baseCameraTable;
            Buffer.MemoryCopy(PluginState.CameraSettings.CameraManagerAddress.ToPointer(), &baseCameraTable, sizeof(IntPtr*), sizeof(IntPtr*));
            getCameraZoomDeltaHook = Hook<getCameraZoomDeltaDelegate>.FromAddress(baseCameraTable[28], getCameraZoomDeltaDetour);
            SetCameraHookState(pluginPreferences.AllowZoomAdjustments);
        }

        public void DrawUI()
        {
            PluginUi.Draw();
        }

        public void DrawConfigUI()
        {
            PluginUi.Visible = true;
        }

        public void OnCommand(string command, string args)
        {
            switch (args)
            {
                case "off":
                    characterRefreshManager.CurrentRefreshState.CurrentMode = RefreshMode.DISABLED;
                    characterRefreshManager.ResetCameraDefaults();
                    PluginUi.stopRefreshCommand();
                    break;
                case "interval":
                    characterRefreshManager.CurrentRefreshState.CurrentMode = RefreshMode.INTERVAL;
                    PluginUi.stopRefreshCommand();
                    break;
                case "on":
                case "always":
                    characterRefreshManager.CurrentRefreshState.CurrentMode = RefreshMode.ALWAYS;
                    PluginUi.stopRefreshCommand();
                    break;
                case "refresh":
                    characterRefreshManager.RefreshAllHeights();
                    break;
                default:
                    PluginUi.Visible = true;
                    break;
            }
        }

        public static void SetCameraHookState(bool enabled){
            if (enabled)
            {
                getCameraZoomDeltaHook.Enable();
            }
            else
            {
                getCameraZoomDeltaHook.Disable();
            }
        }

        public void Dispose()
        {
            framework.Update -= OnUpdateEvent;
            clientState.Logout -= onLogoutEvent;
            characterRefreshManager.Dispose();
        }
    }
}
