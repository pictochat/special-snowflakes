﻿using ImGuiNET;
using System;
using System.Collections.Generic;
using System.Numerics;
using Dalamud.Logging;
using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Objects;
using Dalamud.Game.ClientState.Objects.SubKinds;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Game.ClientState.Objects.Enums;
using Dalamud.Hooking;
using System.Timers;
using System.Threading.Tasks;
using System.Threading;

using SpecialSnowflakes.Chara;
using SpecialSnowflakes.Utils;

namespace SpecialSnowflakes.UI
{
    class PluginUI
    {
        private Configuration configuration { get; init; }
        private SSFCharacterManager characterManager { get; init; }
        private CharacterRefreshManager refreshManager { get; init; }
        private PluginPreferences pluginPreferences { get; init; }

        private Character currentCharacter;
        private string newName = "Player Name";
        private float uiScale = 1f;
        private bool visible = false;
        private bool preferencesVisible = false;
        private bool characterRemoved = false;
        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public PluginUI(Configuration configuration, SSFCharacterManager characterManager, CharacterRefreshManager refreshManager, PluginPreferences pluginPreferences)
        {
            this.configuration = configuration;
            this.characterManager = characterManager;
            this.refreshManager = refreshManager;
            this.pluginPreferences = pluginPreferences;
        }

        public void Draw()
        {
            uiScale = Dalamud.Interface.ImGuiHelpers.GlobalScale;
            DrawMainWindow();
            drawPreferencesWindow();
        }
        private unsafe void addCharacter(string characterName)
        {
            if (characterManager.AddCharacter(characterName))
            {
                PluginLog.Debug($"Found {characterName}!");
                newName = "Player Name";
            }
            else
            {
                PluginLog.Debug($"{characterName} was not added.");
            }
            if (refreshManager.CurrentRefreshState.CurrentMode == RefreshMode.ALWAYS) characterManager.InitQueue();
        }

        private unsafe void removeCharacter(string characterName)
        {
            characterManager.RemoveCharacter(characterName);
            if (refreshManager.CurrentRefreshState.CurrentMode == RefreshMode.ALWAYS) characterManager.InitQueue();
            characterRemoved = true;
        }
        public unsafe void DrawMainWindow()
        {
            if (!visible)
            {
                return;
            }
            ImGui.SetNextWindowSize(new Vector2(500, 500), ImGuiCond.FirstUseEver);
            ImGui.SetNextWindowSizeConstraints(new Vector2(375, 330 * uiScale), new Vector2(float.MaxValue, float.MaxValue));
            if (ImGui.Begin("Special Snowflakes", ref visible, ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoScrollWithMouse))
            {
                ImGui.Columns(2);
                {
                    ImGui.BeginChild("CharacterList");
                    ImGui.InputText("", ref newName, 64); ImGui.SameLine();
                    if (ImGui.Button("Add", new Vector2(50f * uiScale, 22f * uiScale)))
                    {
                        addCharacter(newName);
                        configuration.characterList = characterManager.Characters;
                        configuration.Save();
                    }
                    ImGui.TreePop();
                    foreach (Character character in characterManager.Characters)
                    {
                        var characterListItem = ImGui.TreeNodeEx(character.Name);
                        if (ImGui.IsItemClicked(ImGuiMouseButton.Left))
                        {
                            currentCharacter = character;
                        }
                        if (characterListItem) ImGui.TreePop();
                    }
                    ImGui.EndChild();
                }
                ImGui.SameLine();
                ImGui.NextColumn();
                {
                    ImGui.BeginChild("Character");
                    if (currentCharacter != null)
                    {
                        refreshManager.UpdateCharacterBase(currentCharacter);
                        ImGui.Text(currentCharacter.Name);
                        ImGui.SameLine(ImGui.GetColumnWidth() - 100f * uiScale);
                        if (ImGui.Button("Remove", new Vector2(100f * uiScale, 24f * uiScale)))
                        {
                            removeCharacter(currentCharacter.Name);
                            configuration.characterList = characterManager.Characters;
                            configuration.Save();
                        }
                        ImGui.Separator();
                        ImGui.PushItemWidth(ImGui.GetColumnWidth() - 175f * uiScale);
                        float currentCharacterHeight = currentCharacter.Height;
                        if (ImGui.InputFloat("Character Height", ref currentCharacterHeight))
                        {
                            currentCharacter.Height = currentCharacterHeight;
                            configuration.characterList = characterManager.Characters;
                            configuration.Save();
                        }
                        ImGui.PopItemWidth();
                        ImGui.SameLine();
                        if (ImGui.Button("Apply", new Vector2(ImGui.GetColumnWidth(), 24f * uiScale)))
                        {
                            refreshManager.UpdateHeight(currentCharacter);
                            refreshManager.UpdateSittingHeight(currentCharacter);
                            refreshManager.UpdateCameraHeight(currentCharacter);
                        }
                        bool readOnly = currentCharacter.ReadOnlyHeight;
                        if (ImGui.Checkbox("Do not apply on automatic refreshes.", ref readOnly))
                        {
                            currentCharacter.ReadOnlyHeight = readOnly;
                            configuration.characterList = characterManager.Characters;
                            configuration.Save();
                        }
                        if (ImGui.IsItemHovered())
                        {
                            ImGui.BeginTooltip();
                            ImGui.PushTextWrapPos(ImGui.GetFontSize() * 25.0f);
                            ImGui.TextUnformatted("The height for this character will not be applied by automatic refreshes.\n\nHave this checked if this character is using racial scaling mods.");
                            ImGui.PopTextWrapPos();
                            ImGui.EndTooltip();
                        }
                        bool disableCustomScale = currentCharacter.DisableCustomScale;
                        if (ImGui.Checkbox("Disable custom scale calcuations.", ref disableCustomScale))
                        {
                            if (disableCustomScale == false) currentCharacter.ResetLastKnownPos();
                            currentCharacter.DisableCustomScale = disableCustomScale;
                            configuration.characterList = characterManager.Characters;
                            configuration.Save();
                        }
                        if (ImGui.IsItemHovered())
                        {
                            ImGui.BeginTooltip();
                            ImGui.PushTextWrapPos(ImGui.GetFontSize() * 25.0f);
                            ImGui.TextUnformatted("Scaling modifcations to the root bone will not be used in calculations.\n\nHave this checked if this character is not using a custom scale derived from the root bone.");
                            ImGui.PopTextWrapPos();
                            ImGui.EndTooltip();
                        }
                        byte* characterBase = currentCharacter.GetLastKnownCharacterBase();
                        byte* modelView = currentCharacter.GetLastKnownModelView();
                        if (characterBase != null && modelView != null)
                        {
                            float currentHeight = refreshManager.GetCurrentHeight(modelView);
                            float currentScale = refreshManager.GetCustomScale(modelView);
                            if (currentHeight > 0)
                            {
                                ImGui.SetCursorPosY(ImGui.GetWindowHeight() - 95 * uiScale);
                                ImGui.Text("Current Height: " + currentHeight);
                                if (!disableCustomScale)
                                {
                                    ImGui.Text("Current Custom Scale: " + currentScale);
                                    ImGui.Text("True Height: " + currentHeight * currentScale);
                                }
                            }
                        }
                        else
                        {
                            refreshManager.UpdateCharacterBase(currentCharacter);
                        }
                    }
                    ImGui.EndChild();
                }
            }
            ImGui.SetCursorPosY(ImGui.GetWindowHeight() - 40 * uiScale);
            ImGui.Columns(1);
            ImGui.BeginChild("Bottom Configuration");
            ImGui.Separator();
            if (ImGui.Button("Refresh All Characters", new Vector2(150f * uiScale, 24f * uiScale)))
            {
                refreshManager.RefreshAllHeights();
            }
            ImGui.SameLine(ImGui.GetWindowWidth() - 100f * uiScale);
            if (ImGui.Button("Preferences", new Vector2(100f * uiScale, 24f * uiScale)))
            {
                preferencesVisible = true;
            }
            ImGui.EndChild();
            ImGui.End();
            if (characterRemoved)
            {
                currentCharacter = null;
                characterRemoved = false;
            }
        }

        public void stopRefreshCommand()
        {
            refreshManager.StopRefresh();
            configuration.refreshBoolOption = refreshManager.CurrentRefreshState.CurrentMode;
            configuration.Save();
        }

        public void drawPreferencesWindow()
        {
            if (!preferencesVisible)
            {
                return;
            }
            ImGui.SetNextWindowSize(new Vector2(300 * uiScale, 240 * uiScale), ImGuiCond.Always);
            if (ImGui.Begin("Special Snowflakes Preferences", ref preferencesVisible,
               ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoScrollWithMouse))
            {
                ImGui.Text("Camera Settings");
                bool zoomAdjustments = pluginPreferences.AllowZoomAdjustments;
                if (ImGui.Checkbox("Enable zoom scaling.", ref zoomAdjustments))
                {
                    pluginPreferences.AllowZoomAdjustments = zoomAdjustments;
                    configuration.zoomScaling = zoomAdjustments;
                    configuration.Save();
                    refreshManager.ResetCameraDefaults();
                }
                if (ImGui.IsItemHovered())
                {
                    ImGui.BeginTooltip();
                    ImGui.PushStyleColor(ImGuiCol.Text, new Vector4(255, 0, 0, 1));
                    ImGui.PushTextWrapPos(ImGui.GetFontSize() * 25.0f);
                    ImGui.TextUnformatted("Camera adjustments can provide an unfair advantage to players and is certainly against FINAL FANTASY XIV and SQUARE ENIX Terms of Service. Use at your own discretion.");
                    ImGui.PopTextWrapPos();
                    ImGui.PopStyleColor();
                    ImGui.EndTooltip();
                }
                if (!zoomAdjustments) ImGui.BeginDisabled();
                ImGui.Indent(30f * uiScale);
                bool allowDutyDynamicZoom = pluginPreferences.DisllowDutyZoomAdjustments;
                if (ImGui.Checkbox("Disable during combat or duties.", ref allowDutyDynamicZoom))
                {
                    pluginPreferences.DisllowDutyZoomAdjustments = allowDutyDynamicZoom;
                    configuration.allowDutyDynamicZoom = allowDutyDynamicZoom;
                    configuration.Save();
                }
                ImGui.Indent(-30f * uiScale);
                if (!zoomAdjustments) ImGui.EndDisabled();
                ImGui.Separator();
                ImGui.Text("Automatic Refresh Settings");
                int refreshRadioButton = (int)refreshManager.CurrentRefreshState.CurrentMode;
                if (ImGui.RadioButton("Disable", ref refreshRadioButton, (int)RefreshMode.DISABLED))
                {
                    refreshManager.CurrentRefreshState.CurrentMode = (RefreshMode)refreshRadioButton;
                    configuration.refreshBoolOption = refreshManager.CurrentRefreshState.CurrentMode;
                    configuration.Save();
                    stopRefreshCommand();
                }
                if (ImGui.RadioButton("Always refresh", ref refreshRadioButton, (int)RefreshMode.ALWAYS))
                {
                    refreshManager.CurrentRefreshState.CurrentMode = (RefreshMode)refreshRadioButton;
                    configuration.refreshBoolOption = refreshManager.CurrentRefreshState.CurrentMode;
                    configuration.Save();
                    characterManager.InitQueue();
                    stopRefreshCommand();
                }
                var refreshBoolUpdate = ImGui.RadioButton("Refresh every", ref refreshRadioButton, (int)RefreshMode.INTERVAL);
                int refreshInterval = refreshManager.CurrentRefreshState.IntervalRate;
                ImGui.SameLine();
                if (refreshBoolUpdate)
                {
                    refreshManager.CurrentRefreshState.CurrentMode = (RefreshMode)refreshRadioButton;
                    refreshManager.CurrentRefreshState.IntervalRate = refreshInterval;
                    configuration.refreshBoolOption = refreshManager.CurrentRefreshState.CurrentMode;
                    configuration.Save();
                    stopRefreshCommand();
                }
                ImGui.PushItemWidth(100.0f * uiScale);
                var refreshIntervalUpdate = ImGui.InputInt("seconds", ref refreshInterval);
                if (refreshIntervalUpdate)
                {
                    refreshManager.CurrentRefreshState.IntervalRate = refreshInterval;
                    refreshManager.CurrentRefreshState.CurrentMode = (RefreshMode)refreshRadioButton;
                    if (refreshRadioButton == (int)RefreshMode.INTERVAL) refreshManager.RestartRefresh();
                    configuration.refreshIntervallUpdate = refreshInterval;
                    configuration.Save();
                }
            }
            ImGui.End();
        }

    }
}
