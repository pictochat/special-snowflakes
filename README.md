# Special Snowflakes

**Repository URL has changed!**
* If you have versions 1.0.0.0 to 1.0.0.2 installed, please ___uninstall and delete the plugin before___ using the new repository URL to avoid issues.
* Use `https://gitlab.com/y2_ss/special-snowflakes/-/raw/main/repo.json` as the new custom repository URL.

If you are unable to change the custom repository URL, you can disable SpecialSnowflakes from the XIVLauncher.
1. Open XIVLauncher and click the gear icon.
2. Click on Plugins on the top navigation bar, scroll down to Special Snowflakes 1.0.0.X, right click, and delete.

If you have any other troubles related this issue, pelase communicate here: https://gitlab.com/y2_ss/special-snowflakes/-/issues/2

Originally supposed to be for character customization but more for size fun. 
## Fun things you can do
* Change your character's, friend's, or crush's height! (And keep it that way)
* Have your camera adjust its bounds with your height. (Don't cheat with this please)
* Sit on chairs


## Limitations
* Sitting down looks weird at first. Camera height is a little lower than usual too when sitting (Partially fixed).
* Going smaller than 0.15 makes the camera weird because of its collision box.
* Can break every major patch. I'll try to update

## Plugin Conflicts?
* Mods from Penumbra/TexTools can be used by disabling automatic updates for characters you don't want to apply SpecialSnowflakes to. 
* Custom scales from Customize+ and other bone modding tools are used while calculating certain features like camera zoom and chair readjsutments. No camera Y-axis not supported yet! These calculations can be disabled.

## Getting started

### Easier way I guess
1. Open FF14 with XIVLauncher and Dalamud.
2. In game, open `/xlsettings` and add `https://gitlab.com/y2_ss/special-snowflakes/-/raw/main/repo.json` to Custom Plugin Repositories (don't forget to click **+** and **Save**!)
3. Plugin should appear in **All** category of plugins.

### Manual
1. Download the source code .zip and extract it to a safe location.
2. Open FF14 with XIVLauncher and Dalamud.
3. In game, open `/xlsettings` and add the path to `SpecialSnowflakes.dll` to Dev Plugin Locations (don't forget to click **+** and **Save**!)
4. Plugin should appear in `/xlplugins`. More instructions in the plugin description there.
